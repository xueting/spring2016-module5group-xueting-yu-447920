<?php

header("Content-Type: application/json");
ini_set("session.cookie_httponly", 1);
include 'validator.php';

error_log("mingleiopen", 3, "/var/tmp/my-errors.log");
session_start();
if($_SESSION['token'] !== $_POST['token']){
error_log("mingleiopen1", 3, "/var/tmp/my-errors.log");
    echo json_encode(array(
        "fail" => true,
        "message" => "Forgery detected"
    ));
    exit;
} else{
    error_log("openEvent: has token\n", 3, "/var/tmp/my-errors.log");
    if(isset($_SESSION['username'])){
        error_log("openEvent: has username\n", 3, "/var/tmp/my-errors.log");
        if(isset($_POST['event_id']) && $_POST['event_id']!=""){
            error_log("openEvent: has event id\n", 3, "/var/tmp/my-errors.log");
            error_log($_POST['event_id'], 3, "/var/tmp/my-errors.log");
            $id = (int) test_input($_POST['event_id']);
            require_once('connectDB.php');
            $stmt = $mysqli->prepare("select id, title, date, time, tag from events where id=? and username=?");
            if(!$stmt){
                echo json_encode(array(
                    "fail" => true,
                    "message" => "Query failed."
                ));
                exit;
            }
            $stmt->bind_param('is', $id, $_SESSION['username']);
            $stmt->execute();
            $result = $stmt->get_result();
            $events = array();
            while ($row = $result->fetch_assoc()){
                $id = preg_match('/[0-9]+/', htmlentities($row['id'])) ? htmlentities($row['id']) : "#000000";
                $title = preg_match('/[0-9a-z]+/', htmlentities($row['title'])) ? htmlentities($row['title']) : "#000000";
                $date = preg_match('/\d{4}-\d{2}-\d{2}/', htmlentities($row['date'])) ? htmlentities($row['date']) : "#000000";
                $time = preg_match('/\d{2}:\d{2}:\d{2}/', htmlentities($row['time'])) ? htmlentities($row['time']) : "#000000";
                $tag = preg_match('/[0-9a-z]+/', htmlentities($row['tag'])) ? htmlentities($row['tag']) : "#000000";
                $events["id"] = $id;
                $events["title"] = $title;
                $events["date"] = $date;
                $events["time"] = $time;
                $events["tag"] = $tag;
            }
            $jsonData = array("fail" => false, "logged" => true, "events" => $events);
            echo json_encode($jsonData);
            $result->free();
            $stmt->close();
            exit;
        }else{
            echo json_encode(array());
            exit;
        }
    } else {
        echo json_encode(array());
        exit;
    }
}
?>
