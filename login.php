<?php

header("Content-Type: application/json");
include 'validator.php';

if (isset($_POST['username']) && isset($_POST['password'])) {
    $userName = (string) test_input($_POST['username']);
    $password = (string) test_input($_POST['password']);
    if (is_null($userName)) {
        echo json_encode(array(
            "success" => false,
            "message" => "Invalid username"
        ));
        exit;
    } else if (is_null($userName)) {
        echo json_encode(array(
            "success" => false,
            "message" => "Invalid password"
        ));
    } else if (strlen($password) < 6) {
        echo json_encode(array(
            "success" => false,
            "message" => "Invalid password, the minimum length should be 6"
        ));
    } else {
        require_once('connectDB.php');
        $stmt = $mysqli->prepare("select count(*), username, password from users where username = ?");
        if(!$stmt){
            echo json_encode(array(
                "success" => false,
                "message" => "Query failed."
            ));
            exit;
        }
        $stmt->bind_param('s', $userName);
        $stmt->execute();
        $stmt->bind_result($cnt, $user_name, $pwd_hash);
        $stmt->fetch();
        if($cnt == 1 && crypt($password, $pwd_hash) == $pwd_hash){
            ini_set("session.cookie_httponly", 1);
            session_start();
            session_destroy();
            ini_set("session.cookie_httponly", 1);
            session_start();
            $_SESSION['username'] = $userName;
            $_SESSION['token'] = substr(md5(rand()), 0, 10); // generate a 10-character random string
            echo json_encode(array(
                "success" => true,
                "token" => $_SESSION['token'],
                "message" => "Login successed."
            ));
            exit;
        }else{
            echo json_encode(array(
                "success" => false,
                "message" => "Username and password doesn't match."
            ));
            exit;
        }
    }
}
?>
