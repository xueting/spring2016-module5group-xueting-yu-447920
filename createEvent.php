<?php

ini_set("session.cookie_httponly", 1);

include 'validator.php';

session_start();

if($_SESSION['token'] != $_POST['token']){
    echo json_encode(array(
        "success" => false,
        "message" => "Forgery detected"
    ));
    exit;
}else{
    $username =(string) test_input($_SESSION['username']);
    if(isset($_POST['username']) && $_POST['username'] != "") {
        $username = (string) test_input($_POST['username']);
    }
    $title = (string) test_input($_POST['title']);
    $date = (string) test_input($_POST['date']);
    $time = (string) test_input($_POST['time']);
    $tag = (string) test_input($_POST['tag']);
    if ($title == "" || $date == "" || $time == "" || $tag == ""){
        echo json_encode(array(
            "success" => false,
            "message" => "Please enter full information"
        ));
        exit;
    }else{
    	error_log("Create Event: insert sql\n", 3, "/var/tmp/my-errors1.log");
        require_once('connectDB.php');
        $stmt = $mysqli->prepare("insert into events (username, title, date, time, tag) values (?, ?, ?, ?, ?)");
        if(!$stmt){
            echo json_encode(array(
                "success" => false,
                "message" => "Insert failed."
            ));
            exit;
        }else{
            $stmt->bind_param('sssss', $username, $title, $date, $time, $tag);
            error_log("bind params\n", 3, "/var/tmp/my-errors.log");
            $stmt->execute();
            $stmt->close();
            echo json_encode(array(
                "success" => true,
            ));
	    error_log("insert successful\n", 3, "/var/tmp/my-errors.log");
            exit;
        }
    }
}
?>
