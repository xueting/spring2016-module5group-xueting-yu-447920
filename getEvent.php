<?php

header("Content-Type: application/json");
ini_set("session.cookie_httponly", 1);
include 'validator.php';

session_start();

if (isset($_SESSION['token']) && $_POST['user_token'] != 0
    && $_SESSION['token'] != $_POST['user_token']) {
    echo json_encode(array(
        "fail" => true,
        "message" => "Forgery detected"
    ));
    exit;
} else {
    if (isset($_SESSION['username']) && $_POST['user_token'] != ""){
        $has_tag = isset($_POST['tag']) && $_POST['tag'] != "";
        require('connectDB.php');
        $username = (string) test_input($_SESSION['username']);
        $sql = "select id, title, date, time, tag from events where username=? order by date, time";
        if ($has_tag) {
            $sql = "select id, title, date, time, tag from events where username=? and tag = ? order by date, time";
        }
        $stmt = $mysqli->prepare($sql);
        if(!$stmt){
            echo json_encode(array(
                "fail" => true,
                "message" => "Query failed."
            ));
            exit;
        }
       if ($has_tag) {
            $tag = (string) test_input($_POST['tag']);
            $stmt->bind_param('ss', $username, $tag);
        } else {
            $stmt->bind_param('s', $username);
        }
        $stmt->execute();
        $result = $stmt->get_result();
        $events = array();
        while ($row = $result->fetch_assoc()){
            $id = preg_match('/[0-9]+/', htmlentities($row['id'])) ? htmlentities($row['id']) : "#000000";
            $title = preg_match('/[0-9a-z]+/', htmlentities($row['title'])) ? htmlentities($row['title']) : "#000000";
            $date = preg_match('/\d{4}-\d{2}-\d{2}/', htmlentities($row['date'])) ? htmlentities($row['date']) : "#000000";
            $time = preg_match('/\d{2}:\d{2}:\d{2}/', htmlentities($row['time'])) ? htmlentities($row['time']) : "#000000";
            $tag = preg_match('/[0-9a-z]+/', htmlentities($row['tag'])) ? htmlentities($row['tag']) : "#000000";
            $event = array();
            $event["id"] = $id;
            $event["title"] = $title;
            $event["date"] = $date;
            $event["time"] = $time;
            $event["tag"] = $tag;
            array_push($events, $event);
        }
        if ($has_tag) {
            $jsonData = array("fail" => false, "logged" => true, "events" => $events, "change_tag" => false);
        } else {
	    $jsonData = array("fail" => false, "logged" => true, "events" => $events, "change_tag" => true);
        }
        echo json_encode($jsonData);
        $result->free();
        $stmt->close();
        exit;
    } else {
        echo json_encode(array(
            "fail" => false,
            "logged" => false,
            "change_tag" => false,
        ));
    }
}

?>
