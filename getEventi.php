<?php

header("Content-Type: application/json");
error_log("baaaaaaa0", 3, "/var/tmp/my-errors.log");
ini_set("session.cookie_httponly", 1);
include 'validator.php';

session_start();
error_log("aaaaaaa1", 3, "/var/tmp/my-errors.log");
if(isset($_SESSION['token']) && $_SESSION['token'] != $_POST['token']){
    error_log("aaaaaaa2", 3, "/var/tmp/my-errors.log");
    echo json_encode(array(
        "fail" => true,
        "message" => "Forgery detected"
    ));
    exit;
}else{
    error_log("aaaaaaa3", 3, "/var/tmp/my-errors.log");
    if(isset($_SESSION['username'])){
        require('connectDB.php');
        $username = (string) test_input($_SESSION['username']);
        $stmt = $mysqli->prepare("select id, title, date, time, tag from events where username=?");
        if(!$stmt){
            echo json_encode(array(
                "fail" => true,
                "message" => "Query failed."
            ));
            exit;
        }
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        $jsonData = array("fail" => false, "logged" => true);
        while ($row = $result->fetch_assoc()){
            $id = preg_match('/[0-9]+/', htmlentities($row['id'])) ? htmlentities($row['id']) : "#000000";
            $title = preg_match('/[0-9a-z]+/', htmlentities($row['title'])) ? htmlentities($row['title']) : "#000000";
            $date = preg_match('/\d{4}-\d{2}-\d{2}/', htmlentities($row['date'])) ? htmlentities($row['date']) : "#000000";
            $time = preg_match('/\d{2}:\d{2}:\d{2}/', htmlentities($row['time'])) ? htmlentities($row['time']) : "#000000";
            $tag = preg_match('/[0-9a-z]+/', htmlentities($row['tag'])) ? htmlentities($row['tag']) : "#000000";
            $jsonData[] = ['id'=>$id,
                            'title'=>$title,
                            'date'=>$date,
                            'time'=>$time,
                            'tag'=>$tag];
        }
        echo json_encode($jsonData);
        $result->free();
        $stmt->close();
        exit;
    } else {
        echo json_encode(array(
            "fail" => false,
            "logged" => false
        ));
    }

}
?>
