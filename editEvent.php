<?php

ini_set("session.cookie_httponly", 1);
include 'validator.php';

session_start();
if($_SESSION['token'] !== $_POST['token']){
    echo json_encode(array(
        "success" => false,
        "message" => "Forgery detected"
    ));
    exit;
}else{
    $username = (string) test_input($_SESSION['username']);
    $id = (int) test_input($_POST['id']);
    $title = (string) test_input($_POST['title']);
    $date = (string) test_input($_POST['date']);
    $time = (string) test_input($_POST['time']);
    $tag = (string) test_input($_POST['tag']);
    if ($title == "" || $date == "" || $time == "" || $tag == ""){
        echo json_encode(array(
            "success" => false,
            "message" => "Please enter full information"
        ));
        exit;
    }else{
        require('connectDB.php');

        $stmt = $mysqli->prepare("update events set title = ?, date = ?, time = ?, tag = ? where id = ? and username = ?");
        if(!$stmt){
            echo json_encode(array(
                "success" => false,
                "message" => "Insert failed."
            ));
            exit;
        }else{
            $stmt->bind_param('ssssis', $title, $date, $time, $tag, $id, $username);
            $stmt->execute();
            $stmt->close();
            echo json_encode(array(
                "success" => true
            ));
            exit;
        }
    }
}
?>