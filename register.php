<?php

header("Content-Type: application/json");
include 'validator.php';

if (isset($_POST['username']) && isset($_POST['password'])) {
    $userName = (string) test_input($_POST['username']);
    $password = (string) test_input($_POST['password']);

    if (is_null($userName)) {
        echo json_encode(array(
            "success" => false,
            "message" => "Invalid username"
        ));
        exit;
    } else if (is_null($password) || strlen($password) < 6) {
        echo json_encode(array(
            "success" => false,
            "message" => "Invalid password, the minimun length should be 6"
        ));
        exit;
    }


    else {
        require_once('connectDB.php');
        $stmt = $mysqli->prepare("select count(*) from users where username = ?");
        if(!$stmt){
            echo json_encode(array(
                "success" => false,
                "message" => "Query failed."
            ));
            exit;
        }
        $stmt->bind_param('s', $userName);
        $stmt->execute();
        $stmt->bind_result($cnt);
        $stmt->fetch();
        $stmt->close();
        if($cnt > 0) {
            echo json_encode(array(
                "success" => false,
                "message" => "Username already exist."
            ));
            exit;
        } else {
            $stmt = $mysqli->prepare("insert into users (username, password) values (?, ?)");
            if(!$stmt){
                echo json_encode(array(
                    "success" => false,
                    "message" => "Insert failed."
                ));
                exit;
            }else{
                $pwd_hash = (string) crypt($password);
                $stmt->bind_param('ss', $userName, $pwd_hash);
                $stmt->execute();
                $stmt->close();
                echo json_encode(array(
                    "success" => true,
                    "message" => "Register successed!"
                ));
                exit;
            }
        }
    }
}
?>
