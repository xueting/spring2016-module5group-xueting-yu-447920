var curDay = new Date();
var curMonth = new Month(curDay.getFullYear(), curDay.getMonth());
var cal_days_labels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
var cal_months_labels = ['January', 'February', 'March', 'April',
    'May', 'June', 'July', 'August', 'September',
    'October', 'November', 'December'];

function get_today() {
  var dd = curDay.getDate();
  var mm = curDay.getMonth() + 1;
  var yyyy = curDay.getFullYear();
  if(dd<10) {
      dd='0'+dd;
  } 

  if(mm<10) {
      mm='0'+mm;
  } 
  return yyyy + '-' + mm + '-' + dd;
}

function prev_month() {
    curMonth = curMonth.prevMonth();
    generateCalendar();
}

function next_month() {
    curMonth = curMonth.nextMonth();
    generateCalendar();
}

function cur_month() {
    curDay = new Date();
    curMonth = new Month(curDay.getFullYear(), curDay.getMonth());
    generateCalendar();
}

document.addEventListener('DOMContentLoaded', function () {
    var btn = document.getElementById('prev_month');
    if (btn) {
        btn.addEventListener('click', prev_month);
    }
    btn = document.getElementById('next_month');
    if (btn) {
        btn.addEventListener('click', next_month);
    }
    btn = document.getElementById('cur_month');
    if (btn) {
        btn.addEventListener('click', cur_month);
    }
});

function generateCalendar() {
    var user_token = token;
    $.post(
        "getEvent.php",
        {user_token:user_token},
        function(jsonData){
            console.log("generateCalendar1");
            if (jsonData.fail === true) {
                alert("Get events failed." + jsonData.message);
            }else{
                console.log("generateCalendar");
                updateCalendar(jsonData);
            }
        },"json");
}

var row;
var i;
var d;
function updateCalendar(data) {
    $("#month").html('<h4 align="center">' + cal_months_labels[curMonth.month] + ", " + curMonth.year + '</h4>');

    var header = '<tr class = "calendarHeader">';
    for (i = 0; i <= 6; i++) {
        header += '<th><h4>'+ cal_days_labels[i] + '</h4></th>';
    }
    header += '</tr>';
    $("#calendar").html(header);
    /*add tags array*/
    var tags = [];
    var today = get_today();
    var findRecent = false;
    var weeks = curMonth.getWeeks();
    for (i = 0; i < weeks.length; i++){
        row = '<tr>';
        var days = weeks[i].getDates();
        for (d = 0; d < days.length; d++) {
            if (days[d].getMonth() != curMonth.month) {
                row += '<td></td>';
            } else if (data.logged === false) {
                row += '<td class ="info"><h4>' + days[d].getDate() + '</h4></td>';
            } else {
                row += '<td class ="info"><h4>' + days[d].getDate() + '</h4>';

                var year = curMonth.year;
                var month = 1 + curMonth.month;
                if (month < 10) {
                    month = '0' + month;
                }
                var day = days[d].getDate();
                if (day < 10) {
                    day = '0' + day;
                }
                var cal_date = year + '-' + month + '-' + day;
                var events = data.events;
                for (var j = 0; j < events.length; j++) {
		            if ($.inArray(events[j].tag, tags) == -1) {
                    	tags.push(events[j].tag);
                    }
		            if (events[j].date == cal_date) {
                    	row += '<li class ="line_event" value=' + events[j].id + '>' + events[j].title + '</li>';
                    }
		            if (findRecent === false && events[j].date >= today) {
			            findRecent = true;
                        var recentList = '<h6 style="color:blue;" class = "recent_event">' + "Hi, your next event is " + events[j].title + " on "+ events[j].date +", at " + events[j].time+'</h6>';
                        $("#recent_event").html(recentList);
                    }	
                }
                if (cal_date >= today) {
                  row += '<button class="create_new btn btn-info" value=' + days[d].getDate() + '>new event</button>' + '</td>';
                }
            }
        }
        row += '</tr>';
        $("#calendar").append(row);
	    if (findRecent === false) {
	        $("#recent_event").html("<h6>You don't have any event coming, add something!</h6>");
	    }
    }

    /*create button for diff tags*/
    if (data.logged === true && data.change_tag === true) {
        $("#tags").empty();
        row = '<h4>Tags : </h4>';
        for (var k = 0; k < tags.length; k++) {
            if (k !== 0 && k%4 === 0) {
                row += '<br>';
            }
            row += '<button class ="tag btn btn-primary" style="width:120px;margin:5px;" value ='+ tags[k] + '>' + tags[k] + '</button>';
        }
        row += '<button class ="tag btn btn-primary" style="width:120px;margin:5px;" value="">All Events</button>';
        $("#tags").html(row);
    } else if (data.logged === false) {
        console.log("logged out");
        $("#tags").empty();
    }

    //Edit Event button
    $("li.line_event").click(function() {
        var id = $(this).attr("value");
        $.post(
            "openEvent.php",
            {token:token, event_id:id},
            function(data){
                if (data.fail === true) {
                    alert("Edit failed." + data.message);
                }else{
                    var events = data.events;
                    $("#editid").val(id);
                    $("#edit_date").attr('placeholder', events.date);
                    $("#edit_time").attr('placeholder', events.time.substring(0,5));
                    $("#edit_title").attr('placeholder', events.title);
                    $("#edit_tag").attr('placeholder', events.tag);
                    $("#edit_date").val(events.date);
                    $("#edit_time").val(events.time.substring(0,5));
                    $("#edit_title").val(events.title);
                    $("#edit_tag").val(events.tag);
                    $("#edit_event_view" ).dialog( "open" );
                }
            },"json");
    });

    $("button.create_new").click(function() {
	var year = curMonth.year;
        var month = 1 + curMonth.month;
        if (month < 10) {
            month = '0' + month;
        }
        var day = $(this).attr("value");
        if (day < 10) {
            day = '0' + day;
        }
        $("#date").html(year + '-' + month + '-' + day);
        $("#time").val('');
        $("#title").val('');
        $("#tag").val('');
        $("#group").val('');
        $("#create_event_view").dialog("open");
    });
/*add function for get tagged event*/

    $("button.tag").click(function() {
        //$(this).css("background-color","#8064A2");
       // $(this).css("border-color","#8064A2");
        var tag = $(this).attr("value");
        $.post(
            "getEvent.php",
            {user_token:token, tag:tag},
            function(data) {
                if (data.fail === true) {
                    alert("Get events failed." + data.message);
                }else{
                    updateCalendar(data);
                }
            },"json");
    });

}

