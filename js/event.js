function create_event() {
    var title = $("#title").val() + "";
    var date = $("#date").text() + "";
    var time = $("#time").val() + ':00';
    var tag = $("#tag").val() + "";
    var group = $("#group").val() + "";
    $.post(
        "createEvent.php",
        {token:token, title:title, date:date, time:time, tag:tag},
	 function(msg){
            if (msg.success) {
                $("#create_event_view" ).dialog("close");
                generateCalendar();
            }else{
                alert("Sorry: " + msg.message);
            }
        },"json");
    //Create new event to a group of users
    if (group !== "") {
        var users = $("#group").val().split(',');
        for (var i = 0; i < users.length; i++) {
            $.post("createEvent.php",
                {token:token, username:users[i], title:title, date:date, time:time, tag:tag},
                function(msg){
                    if (msg.success) {
                        alert("insert succeed");
                    }else{
                        alert("Sorry:" + msg.message);
                    }
                },"json");
        }
    }
}

function edit_event() {
    var id = $("#editid").val();
    var title = $("#edit_title").val() + "";
    var date = $("#edit_date").val() + "";
    var time = $("#edit_time").val() + "";
    var tag = $("#edit_tag").val() + "";
    $.post(
        "editEvent.php",
        {token:token, id:id, title:title, date:date, time:time, tag:tag},
        function(msg){
            if (msg.success) {
                $( "#edit_event_view" ).dialog( "close" );
                generateCalendar();
            }else{
                alert("Sorry:" + msg.message);
            }
        },"json");
}

function delete_event() {
    var id = $("#editid").val();
    console.log(id + "this is the event id");
    $.post(
        "deleteEvent.php",
        {token:token, id:id},
        function(msg){
            if (msg.success === true) {
		console.log("msg received from php");
                $( "#edit_event_view" ).dialog( "close" );
                generateCalendar();
		console.log("envoke generateCal() for deletion");
            }else{
                alert("Sorry:" + msg.message);
            }
        },"json");
}

document.addEventListener('DOMContentLoaded', function () {
    var event = document.getElementById('create_event');
    if (event) {
        event.addEventListener('click', create_event);

    }
    event = document.getElementById('edit_event');
    if (event) {
        event.addEventListener('click', edit_event);
    }
    event = document.getElementById('delete_event');
    if (event) {
        event.addEventListener('click', delete_event);
    	console.log("clik delete_event button");
    }
    $( "#create_event_view" ).dialog({ hide:true, autoOpen: false });
    $( "#edit_event_view" ).dialog({ hide:true, autoOpen: false });
});

