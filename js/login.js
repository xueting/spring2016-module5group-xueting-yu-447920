var token = "";

function login() {
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    $.post(
        "login.php",
        {username: username, password: password},
        function(msg){
            if (msg.success) {
                $(".loginview").hide();
                $("#username_label").html(username);
                $(".logoutview").show();
                token = msg.token;
                console.log("token after login is:" + token);
                generateCalendar();
            }else{
                alert("Sorry: " + msg.message);
            }
        },"json");

    document.getElementById('username').value = '';
    document.getElementById('password').value = '';
}

function register() {
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    $.post(
        "register.php",
        {username: username, password: password},
        function(msg){
            if (msg.success === true) {
		alert("Register Success, you can log in now");
		/*login()*/
            }else{
                alert("Sorry: " + msg.message);
            }
        },"json");
    document.getElementById('username').value = '';
    document.getElementById('password').value = '';
}

function logout() {
    token = "";
    console.log("logout making token 0");
    $.post(
        "logout.php",
        function(msg){
            if (msg.success === true) {
                $(".logoutview").hide();
                $(".loginview").show();
                generateCalendar();
		console.log("invoke function generate Calendar()");
            }else{
                alert("Sorry: " + msg.message);
            }
        },"json");
}

document.addEventListener('DOMContentLoaded', function () {
    var btn = document.getElementById('login');
    if (btn) {
        btn.addEventListener('click', login);
    }
    btn = document.getElementById('register');
    if (btn) {
        btn.addEventListener('click', register);
    }
    btn = document.getElementById('logout');
    if (btn) {
        btn.addEventListener('click', logout);
    }
});
